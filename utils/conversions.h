#ifndef CONVERSIONS_H
#define CONVERSIONS_H

#include "../os_assignment_2.h"
#include "errors.h"

/* Check if a value is in the range of numbers. */
#define IN_RANGE(val, min, max) (val >= min && val <= max)

long to_long(char *str);

#endif
