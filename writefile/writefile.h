#ifndef WRITEFILE_H
#define WRITEFILE_H

#include "../os_assignment_2.h"
#include "../utils/errors.h"
#ifdef DEBUG
    #include "../utils/debug.h"
#endif

void *save_lines(void *p_out);

#endif
