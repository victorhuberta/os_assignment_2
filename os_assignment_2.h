#ifndef OS_ASSIGNMENT_2_H
#define OS_ASSIGNMENT_2_H

#define _XOPEN_SOURCE

#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

extern pthread_mutex_t inf_mtx; /* Input file mutex */
extern pthread_mutex_t outf_mtx; /* Output file mutex */

extern pthread_mutex_t lt_buf_r_mtx; /* Shared buffer mutex for read */
extern pthread_mutex_t lt_buf_w_mtx; /* Shared buffer mutex for write */

extern pthread_mutex_t check_taken_mtx; /* Check if line is taken mutex */
extern pthread_cond_t not_taken_cond; /* Condition for not taken */

extern pthread_mutex_t check_eof_mtx; /* Check EOF mutex */
extern pthread_cond_t not_eof_cond; /* Condition for not EOF */

char **lt_buf; /* Share buffer of lines */
extern int read_i; /* Index of lt_buf read */
extern int write_i; /* Index of lt_buf write */
#define N_MAX_LINES 16
#define RESET_INDEX(i) i = (i >= N_MAX_LINES) ? 0 : i

extern int eof_flag;

#endif
