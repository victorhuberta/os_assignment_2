/**
 * Module: writefile
 * Author: Victor Huberta
 * ```
 * Responsible for reading lines from lt_buf
 * (shared buffer of lines) and write them
 * into the output file.
 *
 * Mutexes used:
 * - outf_mtx -> for writing into output file.
 * - lt_buf_r_mtx -> for reading from the lt_buf.
 *
 * - check_eof_mtx -> for checking if the EOF flag
 *   is set.
 *
 * - not_eof_cond -> a condition mutex for blocking if
 *   the EOF flag is not set (which means the writer
 *   thread is faster than the reader thread).
 *
 * All mutexes are declared in the main module.
 */

#include "writefile.h"

void _remove_write_line(FILE *p_out, char *line);
void _remove_line(char *line);
void _unblock_reader_threads(void);
void _write_line_to_file(FILE *p_out, char *line);
int _is_eof(void);

/**
 * Save lines from lt_buf (shared buffer) to output file.
 *
 * Algorithm:
 * 1. Check if there is a line to be read at
 *    the current space in lt_buf.
 *    If there is, take line from lt_buf.
 *    Else, check if EOF flag is set.
 *      If it is, terminate.
 *      Else, block until there is a line.
 * 2. Take line from lt_buf (read and remove).
 * 3. Write line into the output file.
 * 4. For every iteration, unblock all reader threads.
 */
void *save_lines(void *p_out)
{
    char *line;

    while (1) {
        pthread_mutex_lock(&lt_buf_r_mtx);
        #ifdef DEBUG
            print_lt_buf("writer");
        #endif
        line = lt_buf[read_i];

        if (line != NULL) {
            _remove_write_line((FILE *) p_out, line);
        } else {
            pthread_mutex_unlock(&lt_buf_r_mtx);
            if (_is_eof()) break;
        }
    }

    _unblock_reader_threads();

    return NULL;
}

/**
 * Remove line from lt_buf and write it
 * onto the output file.
 */
void _remove_write_line(FILE *p_out, char *line)
{
    _remove_line(line);
    _unblock_reader_threads();

    pthread_mutex_lock(&outf_mtx);
    pthread_mutex_unlock(&lt_buf_r_mtx);

    _write_line_to_file(p_out, line);

    pthread_mutex_unlock(&outf_mtx);

    free(line); /* IMPORTANT: Do not remove this */
}

void _remove_line(char *line)
{
    /* Don't need to lock lt_buf_w_mtx, because only
    one writer thread access the lt_buf at a time. */
    if (line == lt_buf[read_i])
        lt_buf[read_i] = NULL;
    read_i++;
    RESET_INDEX(read_i);
}

void _unblock_reader_threads(void)
{
    pthread_mutex_lock(&check_taken_mtx);
    pthread_cond_broadcast(&not_taken_cond);
    pthread_mutex_unlock(&check_taken_mtx);
}

void _write_line_to_file(FILE *p_out, char *line)
{
    if (fprintf(p_out, "%s", line) < strlen(line)) {
        pthread_mutex_unlock(&outf_mtx);
        fprintf(stderr,
            "Failed to write line properly. Aborting...\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * Block if the EOF flag is not set. Else, terminate.
 */
int _is_eof(void)
{
    pthread_mutex_lock(&check_eof_mtx);
    if (eof_flag) {
        pthread_mutex_unlock(&check_eof_mtx);
        return 1;
    }
    pthread_cond_wait(&not_eof_cond, &check_eof_mtx);
    pthread_mutex_unlock(&check_eof_mtx);

    return 0;
}
