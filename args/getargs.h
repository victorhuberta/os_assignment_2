#ifndef GETARGS_H
#define GETARGS_H

#include "../os_assignment_2.h"
#include "../utils/errors.h"
#include "../utils/conversions.h"

#define N_REQUIRED_ARGS 2

#define N_MIN_THREADS 1
#define N_MAX_THREADS 100
#define N_DEFAULT_THREADS 3

void init_args(int count, char **args);
void getargs(FILE **p_input, FILE **p_out, long *pt_num);

#endif
