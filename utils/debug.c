/**
 * Module: debug
 * Author: Victor Huberta
 * ```
 * Responsible for helping the programmer to debug
 * this program by providing utility functions.
 */

#include "debug.h"

pthread_mutex_t print_mtx = PTHREAD_MUTEX_INITIALIZER;

/**
 * Print the content of lt_buf (shared buffer),
 * suitable to be used with short strings.
 */
void print_lt_buf(char *thread_type)
{
    int i;

    pthread_mutex_lock(&print_mtx);
    printf("%s -> ", thread_type);
    for (i = 0; i < N_MAX_LINES; ++i) {
        if (lt_buf[i] == NULL) {
            printf("[null]");
        } else {
            lt_buf[i][strlen(lt_buf[i]) - 1] = '\0';
            printf("[%s]", lt_buf[i]);
            lt_buf[i][strlen(lt_buf[i])] = '\n';
        }
    }
    puts("");
    pthread_mutex_unlock(&print_mtx);
}
